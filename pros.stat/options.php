<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();
defined('ADMIN_MODULE_NAME') or define('ADMIN_MODULE_NAME', 'pros.stat');

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Text\String;

if (!$USER->isAdmin()) {
    $APPLICATION->authForm('Nope');
}

$app = Application::getInstance();
$context = $app->getContext();
$request = $context->getRequest();

Loc::loadMessages($context->getServer()->getDocumentRoot()."/bitrix/modules/main/options.php");
Loc::loadMessages(__FILE__);

$tabControl = new CAdminTabControl("tabControl", array(
    array(
        "DIV" => "edit1",
        "TAB" => Loc::getMessage("MAIN_TAB_SET"),
        "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_SET"),
    ),
));


$arOptionsNames = array(
	"target_id",
	"target_id_2",
	"target_id_3",
);
if ((!empty($save) || !empty($restore)) && $request->isPost() && check_bitrix_sessid()) {
	
	$error = true;
	foreach($arOptionsNames as $optionName){		
		if ($request->getPost($optionName)) {
			Option::set(
				ADMIN_MODULE_NAME,
				$optionName,
				$request->getPost($optionName)
			);
			$error = false;
		}
	}
	
    if (!empty($restore)) {
        Option::delete(ADMIN_MODULE_NAME);
        CAdminMessage::showMessage(array(
            "MESSAGE" => Loc::getMessage("REFERENCES_OPTIONS_RESTORED"),
            "TYPE" => "OK",
        ));
    } elseif ($error == false) {        
        CAdminMessage::showMessage(array(
            "MESSAGE" => Loc::getMessage("REFERENCES_OPTIONS_SAVED"),
            "TYPE" => "OK",
        ));
    } else {
        CAdminMessage::showMessage(Loc::getMessage("REFERENCES_INVALID_VALUE"));
    }
}

$tabControl->begin();
?>

<form method="post" action="<?=sprintf('%s?mid=%s&lang=%s', $request->getRequestedPage(), urlencode($mid), LANGUAGE_ID)?>">
    <?php
    echo bitrix_sessid_post();
    $tabControl->beginNextTab();
    ?>
    <tr>
        <td width="40%">
            <label for="target_id"><?=Loc::getMessage("REFERENCES_MAX_IMAGE_SIZE") ?>:</label>
        <td width="60%">
            <input type="text"
                   size="50"
                   maxlength="25"
                   name="target_id"
                   value="<?=String::htmlEncode(Option::get(ADMIN_MODULE_NAME, "target_id", "statlink"));?>"
                   />
        </td>
    </tr>
	<tr>
        <td width="40%">
            <label for="target_id_2"><?=Loc::getMessage("REFERENCES_MAX_IMAGE_SIZE") ?>_2:</label>
        <td width="60%">
            <input type="text"
                   size="50"
                   maxlength="25"
                   name="target_id_2"
                   value="<?=String::htmlEncode(Option::get(ADMIN_MODULE_NAME, "target_id_2", ""));?>"
                   />
        </td>
    </tr>
	<tr>
        <td width="40%">
            <label for="target_id_3"><?=Loc::getMessage("REFERENCES_MAX_IMAGE_SIZE") ?>_3:</label>
        <td width="60%">
            <input type="text"
                   size="50"
                   maxlength="25"
                   name="target_id_3"
                   value="<?=String::htmlEncode(Option::get(ADMIN_MODULE_NAME, "target_id_3", ""));?>"
                   />
        </td>
    </tr>

    <?php
    $tabControl->buttons();
    ?>
    <input type="submit"
           name="save"
           value="<?=Loc::getMessage("MAIN_SAVE") ?>"
           title="<?=Loc::getMessage("MAIN_OPT_SAVE_TITLE") ?>"
           class="adm-btn-save"
           />
    <input type="submit"
           name="restore"
           title="<?=Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>"
           onclick="return confirm('<?= AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')"
           value="<?=Loc::getMessage("MAIN_RESTORE_DEFAULTS") ?>"
           />
    <?php
    $tabControl->end();
    ?>
</form>
