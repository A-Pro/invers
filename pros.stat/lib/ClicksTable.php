<?php
namespace Pros\Stat;

defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Entity\DatetimeField;
use Bitrix\Main\Entity\Validator;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class StatClicksTable extends DataManager
{
    public static function getTableName()
    {
        return 'pros_stat_clicks';
    }

    public static function getMap()
    {
        return array(
            new IntegerField('ID', array(
                'autocomplete' => true,
                'primary' => true,
                'title' => Loc::getMessage('ID'),
            )),
            new StringField('IP_ADDRESS', array(
                'required' => true,
                'title' => Loc::getMessage('IP_ADDRESS'),                
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
            new IntegerField('OFFER_ID', array(
                'required' => true,
                'title' => Loc::getMessage('OFFER_ID'),                
            )),
			new StringField('TARGET_ID', array(
                'required' => true,
                'title' => Loc::getMessage('TARGET_ID'), 
				'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                },
            )),
			new DatetimeField('ADD_TIME', array(
                'required' => true,
                'title' => Loc::getMessage('IBLOCK_ENTITY_TIMESTAMP_X_FIELD')
            )),
        );
    }
}
