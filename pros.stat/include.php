<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Loader;
use Bitrix\Main\EventManager;

Loader::registerAutoLoadClasses('pros.stat', array(
    'Pros\Stat\StatEntersTable' => 'lib/EntersTable.php',
	'Pros\Stat\StatClicksTable' => 'lib/ClicksTable.php',
	'Pros\Stat\StatTargetsTable' => 'lib/TargetsTable.php',
	'PStatEnters' => 'classes/mysql/enters.php',
	'PStatClicks' => 'classes/mysql/clicks.php',
	'PStatTargets' => 'classes/mysql/targets.php',
	'PStatistics' => 'classes/general/statistics.php',
));

EventManager::getInstance()->addEventHandler('main', 'OnAfterUserAdd', function(){
    // do something when new user added
});
