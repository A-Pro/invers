<?php

class PStatTargets
{
	public function GetList($arFilter=Array(), $bIncCnt = false){
		global $DB;
		$strSqlSearch = "";
		foreach($arFilter as $key => $val)
		{
			switch($key)
			{
			case "ID":
				$sql = "ID = '".$val."'";
				break;
			case "NAME":
				$sql = "NAME = '".$val."'";
				break;
			case "TARGET_ID":
				$sql = "TARGET_ID = '".$val."'";
				break;
			default:
				$sql = "";
				break;
			}
			
			if(strlen($sql))
				$strSqlSearch .= " AND  (".$sql.") ";
		}
		
		if(!$bIncCnt)
		{
			$strSql = "
				SELECT
					E.*					
				FROM
					pros_stat_targets E
				WHERE 1 = 1					
					".$strSqlSearch."
			";
		}
		else
		{
			$strSql = "
				SELECT
					E.*
					,COUNT(E.".$bIncCnt.") as ELEMENT_CNT
				FROM
					pros_stat_targets E					
				WHERE 1 = 1
					".$strSqlSearch."
				GROUP BY E.".$bIncCnt."
			";
		}
		
		$strSqlOrder = " ORDER BY ID";
		$res = $DB->Query($strSql.$strSqlOrder, false, "FILE: ".__FILE__."<br> LINE: ".__LINE__);
		return $res;		
	}
	
	public function Add($arFields){
		global $DB;
		if(strlen($arFields["IP_ADDRESS"] > 0) && strlen($arFields["OFFER_ID"] > 0)){
			$ID = $DB->Add("pros_stat_targets", $arFields);		
			$Result = $ID;
		}else{
			$Result = false;
		}
		
		return $Result;
	}
}