<?php

class PStatClicks
{
	public function GetList($arFilter=Array(), $bIncCnt = false, $limit = false, $format = "day"){
		global $DB;
		$strSqlSearch = "";
		if($format == "day"){
			$format = "%d.%m.%Y";
		}else{
			$format = "%m.%Y";
		}
		foreach($arFilter as $key => $val)
		{
			switch($key)
			{
			case "OFFER_ID":
				//$sql = "OFFER_ID = '".$val."'";
				$arr = explode(",", $val);
				if (!empty($arr))
				{
					$arr = array_map("intval", $arr);
					$str = implode(", ", $arr);
					$sql = "OFFER_ID in (".$str.")";
				}
				break;
			case "IP_ADDRESS":
				$sql = "IP_ADDRESS = '".$val."'";
				break;
			case "TARGET_ID":
				$sql = "TARGET_ID = '".$val."'";
				break;
			case "FROM":
				$sql = "ADD_TIME>=FROM_UNIXTIME('".MkDateTime(FmtDate($val,"D.M.Y"),"d.m.Y")."')";
				break;
			case "TO":
				$sql = "ADD_TIME<=FROM_UNIXTIME('".MkDateTime(FmtDate($val,"D.M.Y")." 23:59:59","d.m.Y H:i:s")."')";
				break;
			default:
				$sql = "";
				break;
			}
			
			if(strlen($sql))
				$strSqlSearch .= " AND  (".$sql.") ";
		}
		
		if(!$bIncCnt)
		{
			$strSql = "
				SELECT
					E.*					
				FROM
					pros_stat_clicks E
				WHERE 1 = 1					
					".$strSqlSearch."
			";
		}
		elseif($bIncCnt == "ADD_TIME")
		{
			$strSql = "
				SELECT
					E.*
					,COUNT(E.".$bIncCnt.") as ELEMENT_CNT
				FROM
					pros_stat_clicks E					
				WHERE 1 = 1
					".$strSqlSearch."
				GROUP BY DATE_FORMAT(E.".$bIncCnt.", '".$format."')
			";
		}else{
			$strSql = "
				SELECT
					E.*
					,COUNT(E.".$bIncCnt.") as ELEMENT_CNT
				FROM
					pros_stat_clicks E					
				WHERE 1 = 1
					".$strSqlSearch."
				GROUP BY ".$bIncCnt."
			";
		}
		
		$strSqlOrder = " ORDER BY ID";
		if($limit){
			$strSqlOrder .= " LIMIT " . $limit;
		}
		$res = $DB->Query($strSql.$strSqlOrder, false, "FILE: ".__FILE__."<br> LINE: ".__LINE__);
		return $res;		
	}
	
	public function Add($arFields){
		global $DB;
		if(strlen($arFields["IP_ADDRESS"] > 0) && strlen($arFields["OFFER_ID"] > 0)){
			$arFields["ADD_TIME"] = new \Bitrix\Main\Type\DateTime(null,0);
			$ID = $DB->Add("pros_stat_clicks", $arFields);		
			$Result = $ID;
		}else{
			$Result = false;
		}
		
		return $Result;
	}
}