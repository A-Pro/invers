<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Pros\Stat\StatEntersTable;
use Pros\Stat\StatClicksTable;
use Pros\Stat\StatTargetsTable;

Loc::loadMessages(__FILE__);

if (class_exists('pros_stat')) {
    return;
}

class pros_stat extends CModule
{
    /** @var string */
    public $MODULE_ID;

    /** @var string */
    public $MODULE_VERSION;

    /** @var string */
    public $MODULE_VERSION_DATE;

    /** @var string */
    public $MODULE_NAME;

    /** @var string */
    public $MODULE_DESCRIPTION;

    /** @var string */
    public $MODULE_GROUP_RIGHTS;

    /** @var string */
    public $PARTNER_NAME;

    /** @var string */
    public $PARTNER_URI;

    public function __construct()
    {
        $this->MODULE_ID = 'pros.stat';
        $this->MODULE_VERSION = '1.0.0';
        $this->MODULE_VERSION_DATE = '2015-06-02 00:00:00';
        $this->MODULE_NAME = Loc::getMessage('MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = "VSL";
        $this->PARTNER_URI = "http://vsl-studio.ru";
    }

    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        $this->installDB();
    }

    public function doUninstall()
    {
        $this->uninstallDB();
        ModuleManager::unregisterModule($this->MODULE_ID);
    }

    public function installDB()
    {
        if (Loader::includeModule($this->MODULE_ID)) {
            StatEntersTable::getEntity()->createDbTable();
			StatClicksTable::getEntity()->createDbTable();
			StatTargetsTable::getEntity()->createDbTable();
        }
    }

    public function uninstallDB()
    {
        if (Loader::includeModule($this->MODULE_ID)) {
            $connection = Application::getInstance()->getConnection();
            $connection->dropTable(StatEntersTable::getTableName());
			$connection->dropTable(StatClicksTable::getTableName());
			$connection->dropTable(StatTargetsTable::getTableName());
        }
    }
}
