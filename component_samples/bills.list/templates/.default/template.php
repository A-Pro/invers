<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
	
	//pre_debug($arResult);
?>
<table class="agents-table">
	<tr>
		<th class="table-th-id">ID</th>		
		<th class="">Номер счёта</th>		
		<th class="">Сумма счёта</th>
		<th class="table-th-date">Дата выставления</th>
		<th class="table-th-id">Оплата</th>
	</tr>	
<?foreach($arResult["BILLS"] as $arBill){
	?>
	<tr>
		<td><a href="/personal/account/make/?ORDER_ID=<?=$arBill["ID"]?>" target="blank"><?echo $arBill["ID"]?></a></td>
		<td><?echo $arBill["PROPERTY_NUMBER_VALUE"] . "-АП"?></td>
		<td><?echo $arBill["PROPERTY_SUM_VALUE"] . " руб"?></td>
		<td><?echo $arBill["DATE_CREATE"]?></td>
		<td>
		<?if($arBill["PROPERTY_BLOCKED_VALUE"] !== 'да'){?>
			<a href="/personal/account/make/?ORDER_ID=<?=$arBill["ID"]?>" target="blank">Оплатить</a>
		<?}else{
			echo 'Оплачен';
		}?>
		
		</td>
	</tr>
	<?
}

?>
</table>
<?echo $arResult['NAV_STRING'];?>


