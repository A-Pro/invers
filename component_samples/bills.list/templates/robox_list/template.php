<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
	
	//pre_debug($arResult);
?>
<table class="agents-table">
	<tr>
		<th class="table-th-id">ID</th>		
		<th class="">Сумма счёта</th>
		<th class="table-th-date">Дата выставления</th>
		<th class="">Оплата</th>
	</tr>	
<?foreach($arResult["BILLS"] as $arBill){
	?>
	<tr>
		<td><?echo $arBill["ID"]?></td>
		<td><?echo $arBill["PROPERTY_SUM_VALUE"] . " руб"?></td>
		<td><?echo $arBill["DATE_CREATE"]?></td>
		
		<?if($arBill["PROPERTY_STATUS_VALUE"] !== 'Оплачен'){?>
		<td class="paylink_<?echo $_GET['order'] ? $_GET['order'] : ''?>">
			<a href="?order_new=yes&prod_id=<?=$arBill["ID"]?>&prod_count=1" data-prod-price="<?=$arBill["PROPERTY_SUM_VALUE"]?>" class="bxmodBuyLink">Оплатить</a>
		<?}else{
		?><td><?
			echo $arBill["PROPERTY_STATUS_VALUE"];
		}?>
		</td>
	</tr>
	<?
}

$APPLICATION->IncludeComponent(
	"bxmod:robox_topay", 
	".default", 
	array(
		"ORDER_IBLOCK_TYPE" => "add_data",
		"PROD_IBLOCK_TYPE" => "add_data",
		"PROD_ID" => $_REQUEST["prod_id"],
		"PROD_COUNT" => $_REQUEST["prod_count"],
		"ORDER_ID" => $_REQUEST["order_id"],
		"ORDER_NEW" => "order_new",
		"PAY_TYPE" => "",
		"ORDER_IBLOCK_ID" => "29",
		"ORDER_PROP_USER" => "148",
		"ORDER_PROP_PROD" => "149",
		"ORDER_PROP_PROD_COUNT" => "151",
		"PROD_IBLOCK_ID" => "30",
		"PROD_PROP_PRICE" => "152",
		"PROD_PROP_COUNT" => "154"
	),
	false
);
?>
</table>
<?echo $arResult['NAV_STRING'];?>


