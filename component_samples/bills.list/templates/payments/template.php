<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
	
	pre_debug($arResult);
?>
<table class="agents-table">
	<tr>
		<th class="table-th-id">ID</th>		
		<th class="">Сумма списания</th>
		<th class="table-th-date">Дата списания</th>
		<th class="table-th-date">Статья списания</th>
	</tr>	
<?foreach($arResult["BILLS"] as $arBill){
	?>
	<tr>
		<td><?echo $arBill["ID"]?></td>
		<td><?echo $arBill["PROPERTY_SUM_VALUE"] . " руб"?></td>
		<td><?echo strlen($arBill["PROPERTY_DATE_VALUE"])>0 ? $arBill["PROPERTY_DATE_VALUE"] : $arBill["DATE_CREATE"]?></td>
		<td><?echo $arBill["PREVIEW_TEXT"]?></td>
	</tr>
	<?
}
?>
</table>
<?echo $arResult['NAV_STRING'];?>


