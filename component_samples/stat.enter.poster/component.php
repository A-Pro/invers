<?
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CUserTypeManager $USER_FIELD_MANAGER
 * @param array $arParams
 * @param CBitrixComponent $this
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

CModule::IncludeModule("pros.stat");
$ip = PStatistics::GetRealIp();
$arFieldsToGet = array(
	"IP_ADDRESS"=>$ip,
	"OFFER_ID"=>$arParams["OFFER_ID"],
	"FROM"=>date("d.m.Y"),
	"TO"=>date("d.m.Y")
);

$arFieldsToAdd = array(
	"IP_ADDRESS"=>$ip,
	"OFFER_ID"=>$arParams["OFFER_ID"],
	"TARGET_ID"=>$arParams["CLICKER"]
);

//�������� ������ ���� �������� � ��� �� IP
$dbRes = PStatEnters::GetList($arFieldsToGet);
if(!$arRes = $dbRes->Fetch()){
	PStatEnters::Add($arFieldsToAdd);
}

$this->IncludeComponentTemplate();
