<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arComponentParameters = array(
	"PARAMETERS" => array(		
		"PAGE_VAR"=>array(
			"NAME" => GetMessage("PAGE_VAR"),
			"TYPE" => "STRING",
			"DEFAULT" => "N",
		),
	),
);
?>