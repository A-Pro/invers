<?
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CUserTypeManager $USER_FIELD_MANAGER
 * @param array $arParams
 * @param CBitrixComponent $this
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$arResult = array();
$arResult["ERROR"] = true;
$arFilter = array();
$FilterParams = array(
	"OFFER_ID", "IP_ADDRESS", "FROM", "TO"
);
foreach($FilterParams as $param){
	if($arParams[$param]){
		$arFilter[$param] = $arParams[$param];
	}
}

if(strlen($arParams["OFFER_ID"]) > 0){
	$dbRes = PStatEnters::GetList($arFilter, $arParams["GROUP_FIELD"], $arParams["LIMIT"], $arParams["FORMAT_DATE"]);
	while($res = $dbRes->GetNext()){
		$arResult["ITEMS"][] = $res;
		$arResult["ERROR"] = false;
	}
}
$this->IncludeComponentTemplate();
