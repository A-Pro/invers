<?
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CUserTypeManager $USER_FIELD_MANAGER
 * @param array $arParams
 * @param CBitrixComponent $this
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if(strlen($_REQUEST['email']) > 0){
	$user = CUser::GetList(
		$by="id",
		$order="desc",
		array("EMAIL"=>$_REQUEST['email'], "ACTIVE"=>"N"),
		array()
	);
	if($arUser = $user->Fetch()){
		$arResult["strProfileInfo"] = GetMessage("EMAIL_EXISTS");
		$arFields = array(
			"EMAIL"=>$_REQUEST['email']
		);
		$event = new CEvent;
		$event->Send("SEND_INFO_TO_ADMIN", SITE_ID, $arFields, "Y", 9);
	}else{
		$user = CUser::GetList(
			$by="id",
			$order="desc",
			array("EMAIL"=>$_REQUEST['email'], "ACTIVE"=>"Y"),
			array()
		);
		if($arUser = $user->Fetch()){
			$arResult["strProfileInfo"] =  GetMessage("USER_ACTIVE");
		}else{
			$arResult["strProfileError"] = GetMessage("USER_DOESNT_EXIST");
		}
	}
}else{
	$arResult["strProfileError"] = GetMessage("EMAIL_DOESNT_EXIST");
}


$this->IncludeComponentTemplate();
