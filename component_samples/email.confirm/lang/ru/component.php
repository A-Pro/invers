<?
$MESS["ACCOUNT_UPDATE"] = "Администратор сайта изменил ваши регистрационные данные.";
$MESS["PROFILE_DEFAULT_TITLE"] = "Профиль пользователя";
$MESS["USER_DONT_KNOW"] = "(неизвестно)";
$MESS["main_profile_sess_expired"] = "Ваша сессия истекла, повторите попытку.";
$MESS["main_profile_decode_err"] = "Ошибка при дешифровании пароля (#ERRCODE#).";
$MESS["USER_DOESNT_EXIST"] = "Пользователь с указанным email не найден";
$MESS["EMAIL_DOESNT_EXIST"] = "Email не найден";
$MESS["EMAIL_EXISTS"] = "Email подтверждён. После проверки модератором Ваш профиль будет активирован.";
$MESS["USER_ACTIVE"] = "Пользователь уже активирован.";
?>