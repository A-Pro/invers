<?
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CUserTypeManager $USER_FIELD_MANAGER
 * @param array $arParams
 * @param CBitrixComponent $this
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$arResult = array();
if($arParams["DISPLAY_PAGER"])
{
	$arNavParams = array(
		"nPageSize" => $arParams["PER_PAGE"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
		"bShowAll" => $arParams["PAGER_SHOW_ALL"],
	);
	$arNavigation = CDBResult::GetNavParams($arNavParams);
}
else
{
	$arNavParams = array(
		"nTopCount" => $arParams["PER_PAGE"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
	);
	$arNavigation = false;
}

if(empty($arParams["PROPERTIES"])){
	$arParams["PROPERTIES"] = array();
}
$objectsDb = CIBlockElement::GetList(
	array("ID"=>"DESC"), 
	$arParams["FILTER"],
	false,
	$arNavParams,
	$arParams["PROPERTIES"]
	//array("ID", "ACTIVE", "TIMESTAMP_X", "DATE_CREATE", "PROPERTY_NUMBER", "PROPERTY_SUM", "PROPERTY_BLOCKED")
);
$arResult['NAV_STRING'] = $objectsDb->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
			
while($arObjects = $objectsDb->GetNext()){
	$arResult['BILLS'][] = $arObjects;				
}

$this->IncludeComponentTemplate();
