<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
	
	//pre_debug($arResult);
	global $USER;
	
?>
<table class="agents-table">
	<tr>
		<th class="table-th-id">ID</th>		
		<th class="">Действие</th>		
		<th class="">Агент</th>
		<th class="table-th-date">Дата</th>
	</tr>	
<?foreach($arResult["BILLS"] as $arBill){
	$dbUser = $USER->GetByID($arBill["PROPERTY_AGENT_VALUE"]);
	if($arUser=$dbUser->Fetch()){
		$login = $arUser["LOGIN"];
		$name = $arUser["NAME"];
		$last_name = $arUser["LAST_NAME"];
	}
	?>
	<tr>
		<td><?echo $arBill["ID"]?></td>
		<td><?echo $arBill["PROPERTY_ACTION_VALUE"]?></td>
		<td><?echo $name ." ". $last_name ." (". $login . ")"?></td>
		<td><?echo $arBill["DATE_CREATE"]?></td>
	</tr>
	<?
}
?>
</table>
<?echo $arResult['NAV_STRING'];?>


