<div class="multimedia_body">
	<?
	$files_calculator=$arResult['PROPERTIES']['MULTIMEDIA_PROMOTIONAL_MATERIALS']['VALUE'];	
	if($files_calculator) {
		$count=0;
		foreach ($files_calculator as $file_calculator_id)
		{
			$path_file = CFile::GetPath($file_calculator_id);
			?>
			<a class="file_catalog_link" href="<?=$path_file?>"><?=$arResult['PROPERTIES']['MULTIMEDIA_PROMOTIONAL_MATERIALS']['DESCRIPTION'][$count]?></a>
			<?
			$count++;
		}
	}
	?>
	<div class="include_area">
		<?
		$ar_result=CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>33, "CODE"=>"multimedia_promotional_materials", "ACTIVE"=>"Y"), Array("DETAIL_TEXT","CODE","NAME","ACTIVE"));
		if($ar_fields=$ar_result->GetNext()){
			if($ar_fields['DETAIL_TEXT'])
			{
				echo '<div class="include_area_wrap">'.htmlspecialchars_decode($ar_fields['DETAIL_TEXT']).'</div>';
			}
		}
		?>
	</div>
	<div class="include_area">
		<?
		$include_area=$arResult['PROPERTIES']['MULTIMEDIA_PROMOTIONAL_MATERIALS_INCLUDE']['VALUE']['TEXT'];
		if($include_area)
		{
			echo '<div class="include_area_wrap">'.htmlspecialchars_decode($include_area).'</div>';
		}
		?>
	</div>
</div>