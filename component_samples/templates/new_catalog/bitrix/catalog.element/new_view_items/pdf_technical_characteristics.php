<div id="social_buttons_pdf">
	<?
	//Социальные кнопки
	$reviews_block='Y';//Блок с отзывами есть
	$page='item_'.$arResult['ID'];//идентификатор страницы, с которой собираются отзывы
	include ($_SERVER["DOCUMENT_ROOT"].SITE_DIR."social_buttons.php");
	?>
</div>
<div id="content" class="pdf_object">
	<?
	if($arResult['PROPERTIES']['PDF_TECHNICAL_CHARACTERISTICS']["VALUE"]!='')
	{
		$path_file = CFile::GetPath($arResult['PROPERTIES']['PDF_TECHNICAL_CHARACTERISTICS']["VALUE"]);//id файла
		if(IsIE()){?>
			<iframe src="<?=$path_file?>" style="width: 100%" class="frameins"></iframe>
		<?}else{?>
			<object id="pdf_object" data="<?=$path_file?>" type="application/pdf" width="100%" height="0" style="margin:0 auto; position:relative; z-index:1;">
			<?/*<embed width="100%" height="100%" name="plugin" src="<?=$path_file?>" type="application/pdf" wmode="opaque">*/?>
				<param name="wmode" value="opaque" /><!-- это параметр позволяет перекрыть объект -->
				alt: <a href="<?=$path_file?>"><?=GetMessage("NO_SUPPORT");?></a>
			</object>
		<?}
	}
	else
		echo GetMessage("COMING_SOON_TEXT");
	?>
</div>