<div id="social_buttons_pdf">
	<?
	//Социальные кнопки
	$reviews_block='Y';//Блок с отзывами есть
	$page='item_'.$arResult['ID'];//идентификатор страницы, с которой собираются отзывы
	include ($_SERVER["DOCUMENT_ROOT"].SITE_DIR."social_buttons.php");
	?>
</div>
<div id="content" class="pdf_object">
	<?
	if(isset($_GET['protocol_item_id']))
	{
		//$arSelect = array("ID", "NAME", "PROPERTY_PDF_PROTOCOLS_COMMON", "PROPERTY_PDF_PROTOCOLS_ELECTROMAGNETIC", "PROPERTY_PDF_PROTOCOLS_HARMONIC");
		$arSelect = array("ID", "NAME");
		$ar_result=CIBlockElement::GetList(Array("NAME"=>"ASC"), Array("IBLOCK_ID"=>$arResult['IBLOCK_ID'], "ID"=>$_GET['protocol_item_id']), $arSelect);
		if($res=$ar_result->GetNext())
		{
			if($res['CNT']>0)
			{
				$TmpID = $res["ID"]; //шаблон свойства протокола PROT_*
				$arProts = array();
				$Obj = CCatalogProduct::GetByIDEx($TmpID);
				foreach($Obj['PROPERTIES'] as $k => $prop)
				{
					$arTmp = array();
					$arProp = explode('_', $k);							
					if($arProp[0] == 'PROT'){						
						$arTmp = array($k, $prop['NAME'], $Obj['ID']);						
						$arProts[] = $arTmp;				
					}					
				}
				
				foreach($arProts as $prot)
				{
					if(isset($_GET['pdf_protocols']) && $_GET['pdf_protocols']==$prot[0])
					{
						if($Obj['PROPERTIES'][$_GET['pdf_protocols']]['VALUE']!='')
						{
							$path_file = CFile::GetPath($Obj['PROPERTIES'][$_GET['pdf_protocols']]['VALUE']);//id файла
							?>
							<?if(IsIE()){?>
								<iframe src="<?=$path_file?>" style="width: 100%" class="frameins"></iframe>
							<?}else{?>
								<object id="pdf_object" data="<?=$path_file?>" type="application/pdf" width="100%" height="0px;">
									alt: <a href="<?=$path_file?>"><?=GetMessage("NO_SUPPORT");?><?//=$prot[1]?></a>
								</object>
							<?
							}
						}
						else
							echo 'Протоколы испытаний отсутствуют';
					}
				}
			}
		}
	}
	?>
</div>