<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//pre_debug($arResult);

?>
<h4 class="h4">Всего объектов: <?=$arResult["fullObjCounter"]?></h4>
<table class="agents-table">
<tr>
	<th class="table-th-id">ID</th>
	<th class="table-th-name">Агент</th>
	<th class="table-th-name">Телефон</th>
	<th>Объектов</th>
</tr>	
	<?foreach($arResult["AGENTS"] as $agent){?>
		<tr>
			<td><?echo $agent["ID"]?></td>
			<td><?echo $agent["NAME"]." ".$agent["LAST_NAME"]." (".$agent["LOGIN"].")"?></td>
			<td><?echo $agent["PERSONAL_PHONE"]?></td>
			<td><?echo $agent["OBJECTS"]?></td>
		</tr>		
	<?}?>
</table>

<h4 class="h4">Новые объекты:</h4>
<table class="agents-table">
	<tr>
		<th class="table-th-id">ID</th>
		<th class="table-th-name">Название/адрес</th>
		<th class="table-th-date">Дата добавления</th>
	</tr>	
<?foreach($arResult["ADDED_OBJECTS"] as $object){
	?>
	<tr>
		<td><?echo $object["ID"]?></td>
		<td><a href="<?echo $object["DETAIL_PAGE_URL"]?>" target="blank"><?echo $object["NAME"]?></a></td>
		<td><?echo $object["DATE_CREATE"]?></td>
	</tr>
	<?
}
?>
</table>
<?echo $arResult['ADDED_OBJECTS_NAV_STRING'];?>
<h4 class="h4">Обновлённые объекты:</h4>
<table class="agents-table">
	<tr>
		<th class="table-th-id">ID</th>
		<th class="table-th-name">Название/адрес</th>
		<th class="table-th-date">Дата изменения</th>
	</tr>	
<?foreach($arResult["UPDATED_OBJECTS"] as $object){
	?>
	<tr>
		<td><?echo $object["ID"]?></td>
		<td><a href="<?echo $object["DETAIL_PAGE_URL"]?>" target="blank"><?echo $object["NAME"]?></a></td>
		<td><?echo $object["TIMESTAMP_X"]?></td>
	</tr>
	<?
}
?>
</table>
<?echo $arResult['UPDATED_OBJECTS_NAV_STRING'];?>
<h4 class="h4">Архивные объекты:</h4>
<table class="agents-table">
	<tr>
		<th class="table-th-id">ID</th>
		<th class="table-th-name">Название/адрес</th>
		<th class="table-th-date">Дата переноса в архив</th>
		<th class="table-th-date">Причина переноса в архив</th>
	</tr>	
<?foreach($arResult["ARCHIVE_OBJECTS"] as $object){
		?>
		<tr>
			<td><?echo $object["ID"]?></td>
			<td><a href="<?echo $object["DETAIL_PAGE_URL"]?>" target="blank"><?echo $object["NAME"]?></a></td>
			<td><?echo $object["PROPERTY_DATE_ARCHIVE_VALUE"]?></td>
			<td><?echo $object["PROPERTY_WHY_ARCHIVE_VALUE"]?></td>
		</tr>
		<?
}?>
</table>
<?echo $arResult['ARCHIVE_OBJECTS_NAV_STRING'];?>

<div><?echo $agent["ARCHIVE_OBJECTS"];?></div>