<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

$arResult = array();
$arResult['AGENTS'] = array();
$arResult['ADDED_OBJECTS'] = array();
$arResult['UPDATED_OBJECTS'] = array();

if($arParams["DISPLAY_PAGER"])
{
	$arNavParams = array(
		"nPageSize" => $arParams["PER_PAGE"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
		"bShowAll" => $arParams["PAGER_SHOW_ALL"],
	);
	$arNavigation = CDBResult::GetNavParams($arNavParams);
}
else
{
	$arNavParams = array(
		"nTopCount" => $arParams["PER_PAGE"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
	);
	$arNavigation = false;
}


$arGroups = $USER->GetUserGroupArray();
$arInter = array_intersect($arGroups, $arParams["AGENCY_GROUP"]);
if(!empty($arInter)){
	$UID = $USER->GetID();
	$agencyDb = CIBlockElement::GetList(array("SORT"=>"ASC"), array("IBLOCK_ID"=>$arParams["AGENCY_IBLOCK"], "PROPERTY_AGENCY_USER"=>$UID));
	if($arAgency = $agencyDb->Fetch()){		
		$agencyID = $arAgency["ID"];
	
		$filter = array("UF_AGENCY"=>$agencyID, "ACTIVE"=>"Y");
		$params = array(
			"SELECT"=>array("UF_*"), 
			"FIELDS"=>array("ID", "LOGIN", "NAME", "LAST_NAME", "PERSONAL_PHONE")
		);
		$dbAgentsList = $USER->GetList($by="id", $order="asc", $filter, $params);
		
		while($arAgentsList = $dbAgentsList->GetNext()){
			$arResult['AGENTS'][] = $arAgentsList;
		}
		
		$fullObjCounter = 0;
		if(count($arResult['AGENTS']) > 0){
			foreach($arResult['AGENTS'] as $k=>$v){
				//не в архиве
				$curObjCounter = 0;
				$objectsDb = CIBlockElement::GetList(
					array("SORT"=>"ASC"), 
					array("IBLOCK_ID"=>$arParams["OBJECTS_IBLOCK"], "PROPERTY_AGENT"=>$v["ID"]),
					false,
					false,
					array("ID")
				);			
				while($arObjects = $objectsDb->GetNext()){				
					$fullObjCounter++;
					$curObjCounter++;
				}	
				$arResult['AGENTS'][$k]['OBJECTS'] = $curObjCounter;
			}
		
		
			$arAgentsFilter = array();
			$arAgentsFilter["LOGIC"] = "OR";
			foreach($arResult['AGENTS'] as $agent){
				$arAgentsFilter[] = array("PROPERTY_AGENT"=>$agent["ID"]);
			}
		
			//в архиве
		
			$objectsDb = CIBlockElement::GetList(
				array("PROPERTY_DATE_ARCHIVE"=>"DESC"), 
				array(
					"IBLOCK_ID"=>$arParams["OBJECTS_IBLOCK"],
					"!PROPERTY_ARCHIVE"=>false,					 
					$arAgentsFilter
				),
				false,
				$arNavParams,
				array("ID", "IBLOCK_ID", "TIMESTAMP_X", "DATE_CREATE", "IBLOCK_SECTION_ID", "NAME", "PREVIEW_PICTURE", "SHOW_COUNTER", "DETAIL_PAGE_URL", "PROPERTY_ARCHIVE", "PROPERTY_WHY_ARCHIVE", "PROPERTY_DATE_ARCHIVE","PROPERTY_PRICE")
			);
			$arResult['ARCHIVE_OBJECTS_NAV_STRING'] = $objectsDb->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
			
			while($arObjects = $objectsDb->GetNext()){
				$arResult['ARCHIVE_OBJECTS'][] = $arObjects;				
			}
			
			//обновлённые сегодня
			
			$objectsDb = CIBlockElement::GetList(
				array("ID"=>"DESC"), 
				array(
				"IBLOCK_ID"=>$arParams["OBJECTS_IBLOCK"], 
				$arAgentsFilter, 
				"PROPERTY_ARCHIVE"=>false, 
				">TIMESTAMP_X"=>date("d.m.Y").' 00:00:00'
				),
				false,
				$arNavParams,
				array("ID", "IBLOCK_ID", "TIMESTAMP_X", "DATE_CREATE", "IBLOCK_SECTION_ID", "NAME", "PREVIEW_PICTURE", "SHOW_COUNTER", "DETAIL_PAGE_URL", "PROPERTY_ARCHIVE", "PROPERTY_WHY_ARCHIVE", "PROPERTY_DATE_ARCHIVE","PROPERTY_PRICE")
			);
			$arResult['UPDATED_OBJECTS_NAV_STRING'] = $objectsDb->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
			while($arObjects = $objectsDb->GetNext()){
				$arResult['UPDATED_OBJECTS'][] = $arObjects;				
			}
			
			
			//добавленные сегодня
		
			$objectsDb = CIBlockElement::GetList(
				array("ID"=>"DESC"), 
				array(
					"IBLOCK_ID"=>$arParams["OBJECTS_IBLOCK"], 
					$arAgentsFilter, 
					"PROPERTY_ARCHIVE"=>false, 
					">DATE_CREATE"=>date("d.m.Y").' 00:00:00'
				),
				false,
				$arNavParams,
				array("ID", "IBLOCK_ID", "TIMESTAMP_X", "DATE_CREATE", "IBLOCK_SECTION_ID", "NAME", "PREVIEW_PICTURE", "SHOW_COUNTER", "DETAIL_PAGE_URL", "PROPERTY_ARCHIVE", "PROPERTY_WHY_ARCHIVE", "PROPERTY_DATE_ARCHIVE","PROPERTY_PRICE")
			);
			$arResult['ADDED_OBJECTS_NAV_STRING'] = $objectsDb->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
			while($arObjects = $objectsDb->GetNext()){
				$arResult['ADDED_OBJECTS'][] = $arObjects;				
			}
		}
		
		$arResult["fullObjCounter"] = $fullObjCounter;
	}else{
		$arResult["MESSAGE"][] = "Агентство не найдено";
	}
}else{
	$arResult["MESSAGE"][] = "Пользователь не является агентством";
}
$this->IncludeComponentTemplate();



?>