<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arComponentParameters = array(
	"PARAMETERS" => array(		
		"OFFER_ID"=>array(
			"NAME" => GetMessage("OFFER_ID"),
			"TYPE" => "STRING",
			"DEFAULT" => "N",
		),
	),
);
?>