<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
?><h3 class="h3" style="height: 40px;"><?=$arParams['BANNER_NAME']?></h3><?
if(!$arResult["ERROR"]){
	$show = $arResult['ITEMS'][1]['ELEMENT_CNT'];
	$click = $arResult['ITEMS'][0]['ELEMENT_CNT'];
	$effect = $click/$show*100;
	echo 'Показов: ' . $show . '<br/>';
	echo 'Кликов: ' . $click . '<br/>';
	echo 'Эффективность: ' . round($effect, 2) . '%';
}else{
	ShowNote('По указанным данным статистика ещё не собрана');
}
?>