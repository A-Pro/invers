<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
?><h3 class="h3" style="height: 40px;"></h3><?
if(!$arResult["ERROR"]){
	$arPoints = array();
	if($arParams["FORMAT_DATE"] == "day"){
		$format = "d.m.Y";
	}else{
		$format = "m.Y";
	}
	foreach($arResult["ITEMS"] as $k=>$arItem){
		$arPoints[$k]["ADD_TIME"] = FormatDate($format, MakeTimeStamp($arItem["ADD_TIME"], "YYYY-MM-DD HH:MI:SS"));
		$arPoints[$k]["ELEMENT_CNT"] = $arItem["ELEMENT_CNT"];
	}
	$json = CUtil::PhpToJSObject($arPoints);	
	?>
	
	<div class="">	

		<div id="<?=$arParams["TARGET_ID"]?>" style="width: 100%; height: 400px;"></div>

	</div>
	<script type="text/javascript">
	$(function(){
			var chartData = <?=$json?>;

		AmCharts.ready(function() {
			var chart = new AmCharts.AmStockChart();
            chart.pathToImages = "/bitrix/templates/nordhouse/include/libs/amcharts_3_14_4/images/";
				
			chart.dataDateFormat = "DD.MM.YYYY";

			var categoryAxesSettings = new AmCharts.CategoryAxesSettings();
			categoryAxesSettings.minPeriod = "1DD";
			categoryAxesSettings.dateFormats = [{period:'fff',format:'JJ:NN:SS'},
							{period:'ss',format:'JJ:NN:SS'},
							{period:'mm',format:'JJ:NN'},
							{period:'hh',format:'JJ:NN'},
							{period:'DD',format:'DD.MM.YYYY'},
							{period:'WW',format:'DD.MM.YYYY'},
							{period:'MM',format:'MM.YYYY'},
							{period:'YYYY',format:'YYYY'}];
			categoryAxesSettings.equalSpacing = false;
			chart.categoryAxesSettings = categoryAxesSettings;	
			
			
            var dataSet = new AmCharts.DataSet();
            dataSet.dataProvider = chartData;
            dataSet.fieldMappings = [{fromField:"ELEMENT_CNT", toField:"value"}];   
            dataSet.categoryField = "ADD_TIME";          
            chart.dataSets = [dataSet];

            var stockPanel = new AmCharts.StockPanel();
            chart.panels = [stockPanel];

            var panelsSettings = new AmCharts.PanelsSettings();
            panelsSettings.startDuration = 2;
            chart.panelsSettings = panelsSettings;  

            var graph = new AmCharts.StockGraph();
            graph.valueField = "value";
            graph.type = "column";
            graph.fillAlphas = 0.5;
            stockPanel.addStockGraph(graph);

            var chartScrollbarSettings = new AmCharts.ChartScrollbarSettings();
            chartScrollbarSettings.graph = graph;
            chartScrollbarSettings.graphType = "line";
			chartScrollbarSettings.height = 40;
			chartScrollbarSettings.position = "top";
            chart.chartScrollbarSettings = chartScrollbarSettings;

            var chartCursorSettings = new AmCharts.ChartCursorSettings();
            chartCursorSettings.enabled = false;
            chart.chartCursorSettings = chartCursorSettings;

            var periodSelector = new AmCharts.PeriodSelector();
            periodSelector.periods = [{period:"DD", count:1, label:"день"},
                                    {period:"DD", selected:true, count:7, label:"неделю"},
                                    {period:"MM", count:1, label:"месяц"},
									{period:"MM", count:3, label:"квартал"},
                                    {period:"YYYY", count:1, label:"год"},
                                    {period:"MAX", label:"Всё"}];  
			periodSelector.fromText = "Показать с: ";	
			periodSelector.toText = "по: ";
			periodSelector.periodsText = "Показать за: ";
            chart.periodSelector = periodSelector;

			chart.write('<?=$arParams["TARGET_ID"]?>');
		});
	})

	</script>
<?
}else{
	ShowNote('По указанным данным статистика ещё не собрана');
}
?>