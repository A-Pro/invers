<?
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CUserTypeManager $USER_FIELD_MANAGER
 * @param array $arParams
 * @param CBitrixComponent $this
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$arResult = array();
$arResult["ERROR"] = true;
$arFilter = array();
$FilterParams = array(
	"OFFER_ID", "IP_ADDRESS", "FROM", "TO", "TARGET_ID"
);
foreach($FilterParams as $param){
	if($arParams[$param]){
		$arFilter[$param] = $arParams[$param];
	}
}
if(strlen($arParams["OFFER_ID"]) > 0 && !is_array($arParams["TARGET_ID"])){
	$dbRes = PStatClicks::GetList($arFilter, $arParams["GROUP_FIELD"], $arParams["LIMIT"], $arParams["FORMAT_DATE"]);

	while($res = $dbRes->GetNext()){
		$arResult["ITEMS"][] = $res;
		$arResult["ERROR"] = false;
	}
}else if(is_array($arParams["TARGET_ID"])){
	foreach($arParams["TARGET_ID"] as $target){
		$arFilter["TARGET_ID"] = $target;
		$dbRes = PStatClicks::GetList($arFilter, $arParams["GROUP_FIELD"], $arParams["LIMIT"], $arParams["FORMAT_DATE"]);

		while($res = $dbRes->GetNext()){
			$arResult["ITEMS"][] = $res;
			$arResult["ERROR"] = false;
		}
	}
}

$this->IncludeComponentTemplate();
