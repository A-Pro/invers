<?
if( $curl = curl_init() ) {	
	/*пишем ключ авторизации в cookies*/
	if(!isset($_COOKIE['key'])){
		curl_setopt($curl, CURLOPT_URL, 'https://pkasko.com/auth/api?login=vslsitedev@gmail.com&password=invers');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		$out = curl_exec($curl);
		$response = json_decode($out, true);
		setcookie('key', $response['api_key'], time()+60*60*24*13);
		$key = $_COOKIE['key'];
	}else{
		$key = $_COOKIE['key'];
	} 
	/*проверка на успешную авторизацию*/
	if(isset($response['error'])){
		echo $response['error']['message'];
	}else{	
		/*получаем ключ авторизации и отправляем запрос для получения списка авто*/
		curl_setopt($curl, CURLOPT_URL, 'https://pkasko.com/calcservice/cars');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		$headers = array(
			'X-Authorization: '.$key
		);
		curl_setopt($curl, CURLOPT_HTTPHEADER,$headers);
		$out = curl_exec($curl);	
		$response = json_decode($out, true);
		
			/*echo "<pre>";
			print_r($response);
			echo "</pre>";*/
		
		if(isset($response['error'])){
			echo $response['error']['message'];
		}else{			
			//пересоберём массив с моделями для вывода
			$arMake = array();
			foreach($response as $k=>$v){
				$arMake[$v['name']] = $v['models'];
			}			
			
			/*расчёт каско*/
			if(isset($_POST['make'])){	
				/*соберём массив для отправки*/
				$prepost = array();
				$prepost['make'] = $_POST['make'];
				$prepost['model'] = $_POST['model'];
				$prepost['year'] = $_POST['year'];
				$prepost['power'] = $_POST['power'];
				$prepost['price'] = (int)$_POST['price'];
				$prepost['credit'] = (boolean)$_POST['credit'];
				$prepost['carGarantee'] = (boolean)$_POST['carGarantee'];
				$prepost['drivers'] = array();
				
				foreach($_POST['age'] as $k=>$v){
					$prepost['drivers'][$k]['age'] = $v;
					$prepost['drivers'][$k]['experience'] = $_POST['experience'][$k];
					$prepost['drivers'][$k]['sex'] = $_POST['driver_'.$k.'_sex'];					
				}
				
				$prepostJson = json_encode($prepost);				
				
				/*отправляем запрос для расчёта*/
				curl_setopt($curl, CURLOPT_URL, 'https://pkasko.com/kasko/calc?api=1');
				curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $prepostJson);
				$headers = array(
					'X-Authorization: '.$key
				);
				curl_setopt($curl, CURLOPT_HTTPHEADER,$headers);
				$out = curl_exec($curl);	
				$response = json_decode($out, true);
				
				$arKasko = array();
				foreach($response['results'] as $result){
					if($result['info']['code'] !== 'OSAGO' && strlen($result['result']['kasko']['premium']) > 0){			
						$arKasko[] = number_format($result['result']['kasko']['premium'], 0, ',', ' ');
					}
				}
				sort($arKasko);
				
				$minKasko = $arKasko[0];
			}
		}		
	}
	
	curl_close($curl);
  }
?>
<div id="kasko_calc">
	<div id="err_block"></div>
	<form method="post" action="">
		<div class="kasko-auto">
			<p class="calc-title-auto">Ваш автомобиль</p>
			<select name="make" id="make">
				<option>Марка</option>
				<?foreach($arMake as $k=>$v){?>
					<option><?=$k?></option>
				<?}?>
			</select>
			<select name="year">
				<option>Год выпуска</option>
				<option value="2015">2015</option>
				<option value="2014">2014</option>
				<option value="2013">2013</option>
				<option value="2012">2012</option>
				<option value="2011">2011</option>
				<option value="2010">2010</option>
				<option value="2009">2009</option>
				<option value="2008">2008</option>
				<option value="2007">2007</option>
				<option value="2006">2006</option>
				<option value="2005">2005</option>
				<option value="2004">2004</option>
				<option value="2003">2003</option>
			</select>
			<input type="text" name="price" value="" placeholder="Стоимость" />
			<select name="model" id="model">
				<option>Модель</option>
				<?if(isset($_GET['make'])){?>
					<?foreach($arMake[$_GET['make']] as $model){?>
						<option><?=$model['name']?></option>
					<?}?>
				<?}?>
			</select>
			<input type="text" name="power" value="" placeholder="Мощность" />
			
			<div class="clear"></div>						
			<p class="warranty">Автомобиль на гарантии:</p>
			<label class="warranty-label"><input type="radio" name="carGarantee" value="1" />Да</label>
			<label class="warranty-label"><input type="radio" name="carGarantee" value="0" />Нет</label>			
			<p class="credit">Автомобиль взят в кредит:</p>
			<label class="credit-label"><input type="radio" name="credit" value="1" />Да</label>
			<label class="credit-label"><input type="radio" name="credit" value="0" />Нет</label>
		</div>
		<div class="clear"></div>
		<div class="kasko-driver">
			<p class="calc-title-driver">Водитель</p>
			<div class="driver">
				<input type="text" name="age[]" value="" placeholder="Возраст" />
				<input type="text" name="experience[]" value="" placeholder="Стаж" />
				<label class="sex-label"><input type="radio" name="driver_0_sex" value="m" />М</label>
				<label class="sex-label"><input type="radio" name="driver_0_sex" value="w" />Ж</label>
			</div>
			
			<span class="add-driver">+добавить водителя</span>
		</div>
		<div class="clear"></div>
		<div id="price_block">
			<input class="button" type="submit" name="send" value="Рассчитать" />
			<p class="price-label">Стоимость полиса от</p>
			<p class="price"><?echo isset($minKasko) ? $minKasko : '0';?></p>
			<input type="hidden" name="min_price_kasko" id="min_price_kasko" value="<?echo isset($minKasko) ? $minKasko : '0';?>" />
			<section class="currency"><img src="<?=SITE_TEMPLATE_PATH?>/images/2-inv_03.png" alt="" /><span class="show-note">*</span>
			<p class="note">
				Данный расчёт носит исключительно информативный характер. В расчёте не учтены такие параметры как: франшиза, порядок оплаты, переход из одной компании в другую. Данные параметры могут снизить стоимость Вашего полиса. Разобраться в тонкостях и деталях полиса поможет наш менеджер. Вам осталось заполнить контакты и нажать "Заказать полис" и мы свяжемся с Вами в течение 5 минут (в рабочее время).
				<span class="close-note"></span>
				<span class="triangle-note"></span>
			</p>
			</section>
		</div>						
	</form>
	<div class="preload"></div>	
</div>
<script>
$('#kasko_calc form').on('submit', function(){
	post = $(this).serializeArray();
	$(this).css('opacity', 0.5);
	$('.preload').fadeIn();
	$.ajax({
		url: location.href,
		type: 'POST',
		data: post,
		success: function (data) {
			price = $(data).find('.price').html();
			min_price = $(data).find('#min_price_kasko').val();
			err = $(data).find('#err_block').html();				
			if(err){
				alert(err);
			}else{
				$('.price').html(price);
				$('#min_price_kasko').val(min_price);
				$('.note').fadeIn();				
			}
			$('#kasko_calc form').css('opacity', 1);
			$('.preload').fadeOut();			
		},
		error: function (data) {
			console.log(data);
		}
	});
		
	return false;
})

$('#make').on('change', function(){
	var name = $(this).val();
	$('#kasko_calc form').css('opacity', 0.5);
	$('.preload').fadeIn();
	$.get(location.href, {make:name}, function(data){
		html = $(data).find('#model');
		$('#model').html(html);
		$('#model select').trigger('refresh');
		$('#kasko_calc form').css('opacity', 1);
		$('.preload').fadeOut();
	})
})
</script>