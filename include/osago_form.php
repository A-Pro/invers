<?
if( $curl = curl_init() ) {	
	/*пишем ключ авторизации в cookies*/
	if(!isset($_COOKIE['key'])){
		curl_setopt($curl, CURLOPT_URL, 'https://pkasko.com/auth/api?login=vslsitedev@gmail.com&password=invers');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		$out = curl_exec($curl);
		$response = json_decode($out, true);
		setcookie('key', $response['api_key'], time()+60*60*24*13);
		$key = $_COOKIE['key'];
	}else{
		$key = $_COOKIE['key'];
	} 
	/*проверка на успешную авторизацию*/
	if(isset($response['error'])){
		echo $response['error']['message'];
	}else{				
		/*получаем ключ авторизации и отправляем запрос для получения списка городов*/
		curl_setopt($curl, CURLOPT_URL, 'https://pkasko.com/region/list');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		$headers = array(
			'X-Authorization: '.$key
		);
		curl_setopt($curl, CURLOPT_HTTPHEADER,$headers);
		$out = curl_exec($curl);	
		$response = json_decode($out, true);
		if(isset($response['error'])){
			echo $response['error']['message'];
		}else{			
			/*расчёт осаго*/
			if(isset($_POST['city'])){	
				/*соберём массив для отправки*/
				$prepost = array();
				$prepost['city'] = $_POST['city'];
				$prepost['region'] = $_POST['region'];
				$prepost['country'] = 'Российская Федерация';
				$prepost['contractPeriod'] = '1 год';
				$prepost['insurancePeriod'] = $_POST['insurancePeriod'];
				$prepost['carType'] = $_POST['carType'];
				$prepost['power'] = $_POST['power'];
				$prepost['violation'] = false;
				$prepost['trailer'] = false;
				$prepost['owner'] = 'Физическое лицо';//$_POST['owner'];
				$prepost['multidrive'] = (boolean)$_POST['multidrive'];
				$prepost['issueDate'] = date('Y-m-d');
				$prepost['drivers'] = array();	
				if(count($_POST['age']) > 0){
					foreach($_POST['age'] as $k=>$v){
						$prepost['drivers'][$k]['age'] = $v;
						$prepost['drivers'][$k]['experience'] = $_POST['experience'][$k];
						$prepost['drivers'][$k]['sex'] = $_POST['driver_'.$k.'_sex'];
						
						$prepost['drivers'][$k]['fullName'] = 'Ivanov Ivan';
						$prepost['drivers'][$k]['birthdate'] = '1980-12-20';
						$prepost['drivers'][$k]['license'] = array(
							"series"=>"78ТС",
							"number"=>"000872",
							"date"=>"2008-01-20"
						);
					}
				}	
				if($prepost['multidrive']){
					$prepost['carVIN'] = '11111111111111111';	
					$prepost['ownerFullName'] = "Ivanov Ivan";
					$prepost["ownerBirtdate"] = "1980-12-20";  
					$prepost["ownerPassport"] = array(
						"series"=>"4000",                        
						"number"=>"123456"
					);
					$prepost["ownerInn"] = "2222222222";
				}
				
				$prepostJson = json_encode($prepost);				
							
				/*отправляем запрос для расчёта*/
				curl_setopt($curl, CURLOPT_URL, 'https://pkasko.com/osago/calc?api=1');
				curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $prepostJson);
				$headers = array(
					'X-Authorization: '.$key
				);
				curl_setopt($curl, CURLOPT_HTTPHEADER,$headers);
				$out = curl_exec($curl);	
				$response = json_decode($out, true);
				
				/*print_r($response);
				exit();*/
				
				$arKasko = array();
				foreach($response['results'] as $result){
					if(strlen($result['result']['total']) > 0){
						$arKasko[] = number_format($result['result']['total'], 0, ',', ' ');
					}
				}
				sort($arKasko);
				
				$minKasko = $arKasko[0];
			}
		}		
	}
	
	curl_close($curl);
  }
?>
<div id="kasko_calc">
	<div id="err_block"></div>
	<form method="post" action="">
		<div class="kasko-auto osago-auto">
			<p class="calc-title-auto">Ваш автомобиль</p>
			<input type="hidden" name="region" id="region"/>
			<select name="city" id="city" data-placeholder="Регион регистрации">
				<option label="-"></option>				
				<option value="Москва">Москва</option>
				<option value="Санкт-Петербург">Санкт-Петербург</option>
				<option value="Московская область" >Московская область</option>
				<option value="Ленинградская область" >Ленинградская область</option>
				<option value="Республика Крым" >Республика Крым</option>
				<optgroup label="Алтайский край">
					<option value="Барнаул">Барнаул</option>
					<option value="Бийск">Бийск</option>
					<option value="Заринск, Новоалтайск, Рубцовск">Заринск, Новоалтайск, Рубцовск</option>
					<option value="Алтайский край - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Амурская область">
					<option value="Амурская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Благовещенск">Благовещенск</option>
					<option value="Белогорск, Свободный">Белогорск, Свободный</option>
				</optgroup>
				<optgroup label="Архангельская область">
					<option value="Северодвинск">Северодвинск</option>
					<option value="Архангельск">Архангельск</option>
					<option value="Котлас">Котлас</option>
					<option value="Архангельская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Астраханская область">
					<option value="Астрахань">Астрахань</option>
					<option value="Астраханская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Белгородская область">
					<option value="Губкин, Старый Оскол">Губкин, Старый Оскол</option>
					<option value="Белгородская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Белгород">Белгород</option>
				</optgroup>
				<optgroup label="Брянская область">
					<option value="Брянская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Клинцы">Клинцы</option>
					<option value="Брянск">Брянск</option>
				</optgroup>
				<optgroup label="Владимирская область">
					<option value="Муром">Муром</option>
					<option value="Гусь-Хрустальный">Гусь-Хрустальный</option>
					<option value="Владимирская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Владимир">Владимир</option>
				</optgroup>
				<optgroup label="Волгоградская область">
					<option value="Волжский">Волжский</option>
					<option value="Волгоградская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Камышин, Михайловка">Камышин, Михайловка</option>
					<option value="Волгоград">Волгоград</option>
				</optgroup>
				<optgroup label="Вологодская область">
					<option value="0">Вологда</option>
					<option value="spb">Череповец</option>
					<option value="11">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Воронежская область">
					<option value="Воронежская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Воронеж">Воронеж</option>
					<option value="Борисоглебск, Лиски, Россошь">Борисоглебск, Лиски, Россошь</option>
				</optgroup>
				<optgroup label="Еврейская автономная область">
					<option value="Еврейская автономная область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Биробиджан">Биробиджан</option>
				</optgroup>
				<optgroup label="Забайкальский край">
					<option value="2">Чита</option>
					<option value="Забайкальский край - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="5">Краснокаменск</option>
				</optgroup>
				<optgroup label="Ивановская область">
					<option value="Кинешма">Кинешма</option>
					<option value="Шуя">Шуя</option>
					<option value="Иваново">Иваново</option>
					<option value="Ивановская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Иркутская область">
					<option value="Иркутск">Иркутск</option>
					<option value="Ангарск">Ангарск</option>
					<option value="Усолье-Сибирское">Усолье-Сибирское</option>
					<option value="Братск, Тулун, Усть-Илимск, Усть-Кут, Черемхово">Братск, Тулун, Усть-Илимск, Усть-Кут, Черемхово</option>
					<option value="Иркутская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Шелехов">Шелехов</option>
				</optgroup>
				<optgroup label="Кабардино-Балкарская Республика">
					<option value="Кабардино-Балкарская Республика - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Нальчик, Прохладный">Нальчик, Прохладный</option>
				</optgroup>
				<optgroup label="Калининградская область">
					<option value="Калининград">Калининград</option>
					<option value="Калининградская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Калужская область">
					<option value="Калуга">Калуга</option>
					<option value="Обнинск">Обнинск</option>
					<option value="Калужская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Камчатский край">
					<option value="Петропавловск-Камчатский">Петропавловск-Камчатский</option>
					<option value="Камчатский край - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Карачаево-Черкесская Республика">
					<option value="Карачаево-Черкесская Республика">Карачаево-Черкесская Республика</option>
				</optgroup>
				<optgroup label="Кемеровская область">
					<option value="Анжеро-Судженск, Киселевск, Юрга">Анжеро-Судженск, Киселевск, Юрга</option>
					<option value="Кемеровская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Новокузнецк">Новокузнецк</option>
					<option value="Белово, Березовский, Осинники, Прокопьевск, Междуреченск">Белово, Березовский, Осинники, Прокопьевск, Междуреченск</option>
					<option value="Кемерово">Кемерово</option>
				</optgroup>
				<optgroup label="Кировская область">
					<option value="Кирово-Чепецк">Кирово-Чепецк</option>
					<option value="Киров">Киров</option>
					<option value="Кировская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Костромская область">
					<option value="Костромская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Кострома">Кострома</option>
				</optgroup>
				<optgroup label="Краснодарский край">
					<option value="Армавир, Сочи, Туапсе">Армавир, Сочи, Туапсе</option>
					<option value="Белореченск, Ейск, Кропоткин, Крымск, Курганинск, Лабинск, Славянск-на-Кубани, Тимашевск, Тихорецк">Белореченск, Ейск, Кропоткин, Крымск, Курганинск, Лабинск, Славянск-на-Кубани, Тимашевск, Тихорецк</option>
					<option value="Краснодарский край - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Краснодар, Новороссийск">Краснодар, Новороссийск</option>
					<option value="Анапа, Геленджик">Анапа, Геленджик</option>
				</optgroup>
				<optgroup label="Красноярский край">
					<option value="Ачинск, Зеленогорск">Ачинск, Зеленогорск</option>
					<option value="Канск, Лесосибирск, Минусинск, Назарово">Канск, Лесосибирск, Минусинск, Назарово</option>
					<option value="Красноярск">Красноярск</option>
					<option value="Железногорск, Норильск">Железногорск, Норильск</option>
					<option value="Красноярский край - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Курганская область">
					<option value="Шадринск">Шадринск</option>
					<option value="Курган">Курган</option>
					<option value="Курганская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Курская область">
					<option value="Курск">Курск</option>
					<option value="Курская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Железногорск">Железногорск</option>
				</optgroup>
				<optgroup label="Липецкая область">
					<option value="Елец">Елец</option>
					<option value="Липецкая область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Липецк">Липецк</option>
				</optgroup>
				<optgroup label="Магаданская область">
					<option value="Магадан">Магадан</option>
					<option value="Магаданская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Мурманская область">
					<option value="Мурманск">Мурманск</option>
					<option value="Апатиты, Мончегорск">Апатиты, Мончегорск</option>
					<option value="Мурманская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Североморск">Североморск</option>
				</optgroup>
				<optgroup label="Ненецкий автономный округ">
					<option value="Ненецкий автономный округ">Ненецкий автономный округ</option>
				</optgroup>
				<optgroup label="Нижегородская область">
					<option value="Кстово">Кстово</option>
					<option value="Арзамас, Выкса, Саров">Арзамас, Выкса, Саров</option>
					<option value="Нижегородская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Нижний Новгород">Нижний Новгород</option>
					<option value="Балахна, Бор, Дзержинск">Балахна, Бор, Дзержинск</option>
				</optgroup>
				<optgroup label="Новгородская область">
					<option value="Боровичи">Боровичи</option>
					<option value="Великий Новгород">Великий Новгород</option>
					<option value="Новгородская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Новосибирская область">
					<option value="Новосибирск">Новосибирск</option>
					<option value="Искитим">Искитим</option>
					<option value="Куйбышев">Куйбышев</option>
					<option value="Бердск">Бердск</option>
					<option value="Новосибирская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Омская область">
					<option value="Омск">Омск</option>
					<option value="Омская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Оренбургская область">
					<option value="Оренбург">Оренбург</option>
					<option value="Орск">Орск</option>
					<option value="Бугуруслан, Бузулук, Новотроицк">Бугуруслан, Бузулук, Новотроицк</option>
					<option value="Оренбургская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Орловская область">
					<option value="Орел">Орел</option>
					<option value="Орловская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Ливны, Мценск">Ливны, Мценск</option>
				</optgroup>
				<optgroup label="Пензенская область">
					<option value="Заречный">Заречный</option>
					<option value="Пензенская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Пенза">Пенза</option>
					<option value="Кузнецк">Кузнецк</option>
				</optgroup>
				<optgroup label="Пермский край">
					<option value="Соликамск">Соликамск</option>
					<option value="Пермский край - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Лысьва, Чайковский">Лысьва, Чайковский</option>
					<option value="Березники, Краснокамск">Березники, Краснокамск</option>
					<option value="Пермь">Пермь</option>
				</optgroup>
				<optgroup label="Приморский край">
					<option value="Приморский край - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Владивосток">Владивосток</option>
					<option value="Арсеньев, Артем, Находка, Спасск-Дальний, Уссурийск">Арсеньев, Артем, Находка, Спасск-Дальний, Уссурийск</option>
				</optgroup>
				<optgroup label="Псковская область">
					<option value="Псков">Псков</option>
					<option value="Псковская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Великие Луки">Великие Луки</option>
				</optgroup>
				<optgroup label="Республика Адыгея">
					<option value="Республика Адыгея">Республика Адыгея</option>
				</optgroup>
				<optgroup label="Республика Алтай">
					<option value="Республика Алтай - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Горно-Алтайск">Горно-Алтайск</option>
				</optgroup>
				<optgroup label="Республика Башкортостан">
					<option value="Благовещенск, Октябрьский">Благовещенск, Октябрьский</option>
					<option value="Ишимбай, Кумертау, Салават">Ишимбай, Кумертау, Салават</option>
					<option value="Республика Башкортостан - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Уфа">Уфа</option>
					<option value="Стерлитамак, Туймазы">Стерлитамак, Туймазы</option>
				</optgroup>
				<optgroup label="Республика Бурятия">
					<option value="Улан-Удэ">Улан-Удэ</option>
					<option value="Республика Бурятия - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Республика Дагестан">
					<option value="Республика Дагестан - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Буйнакск, Дербент, Каспийск, Махачкала, Хасавюрт">Буйнакск, Дербент, Каспийск, Махачкала, Хасавюрт</option>
				</optgroup>
				<optgroup label="Республика Ингушетия">
					<option value="Малгобек">Малгобек</option>
					<option value="Республика Ингушетия - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Назрань">Назрань</option>
				</optgroup>
				<optgroup label="Республика Калмыкия">
					<option value="Элиста">Элиста</option>
					<option value="Республика Калмыкия - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Республика Карелия">
					<option value="Республика Карелия - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Петрозаводск">Петрозаводск</option>
				</optgroup>
				<optgroup label="Республика Коми">
					<option value="Республика Коми - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Сыктывкар">Сыктывкар</option>
					<option value="Ухта">Ухта</option>
				</optgroup>
				<optgroup label="Республика Марий Эл">
					<option value="Республика Марий Эл - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Волжск">Волжск</option>
					<option value="Йошкар-Ола">Йошкар-Ола</option>
				</optgroup>
				<optgroup label="Республика Мордовия">
					<option value="Саранск">Саранск</option>
					<option value="Республика Мордовия - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Рузаевка">Рузаевка</option>
				</optgroup>
				<optgroup label="Республика Саха (Якутия)">
					<option value="Республика Саха (Якутия) - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Нерюнгри">Нерюнгри</option>
					<option value="Якутск">Якутск</option>
				</optgroup>
				<optgroup label="Республика Северная Осетия - Алания">
					<option value="Владикавказ">Владикавказ</option>
					<option value="Республика Северная Осетия - Алания - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Республика Татарстан">
					<option value="Набережные Челны">Набережные Челны</option>
					<option value="Елабуга">Елабуга</option>
					<option value="Республика Татарстан - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Бугульма, Лениногорск, Чистополь">Бугульма, Лениногорск, Чистополь</option>
					<option value="Альметьевск, Зеленодольск, Нижнекамск">Альметьевск, Зеленодольск, Нижнекамск</option>
					<option value="Казань">Казань</option>
				</optgroup>
				<optgroup label="Республика Тыва">
					<option value="Кызыл">Кызыл</option>
					<option value="Республика Тыва - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Республика Хакасия">
					<option value="Абакан, Саяногорск, Черногорск">Абакан, Саяногорск, Черногорск</option>
					<option value="Республика Хакасия - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Ростовская область">
					<option value="Азов">Азов</option>
					<option value="Шахты">Шахты</option>
					<option value="Волгодонск, Гуково, Каменск-Шахтинский, Новочеркасск, Новошахтинск, Сальск, Таганрог">Волгодонск, Гуково, Каменск-Шахтинский, Новочеркасск, Новошахтинск, Сальск, Таганрог</option>
					<option value="Ростов-на-Дону">Ростов-на-Дону</option>
					<option value="Ростовская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Батайск">Батайск</option>
				</optgroup>
				<optgroup label="Рязанская область">
					<option value="Рязань">Рязань</option>
					<option value="Рязанская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Самарская область">
					<option value="Чапаевск">Чапаевск</option>
					<option value="Новокуйбышевск, Сызрань">Новокуйбышевск, Сызрань</option>
					<option value="Самара">Самара</option>
					<option value="Тольятти">Тольятти</option>
					<option value="Самарская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Саратовская область">
					<option value="Энгельс">Энгельс</option>
					<option value="Саратовская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Балаково, Балашов, Вольск">Балаково, Балашов, Вольск</option>
					<option value="Саратов">Саратов</option>
				</optgroup>
				<optgroup label="Сахалинская область">
					<option value="Южно-Сахалинск">Южно-Сахалинск</option>
					<option value="Сахалинская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Свердловская область">
					<option value="Верхняя Салда, Полевской">Верхняя Салда, Полевской</option>
					<option value="Асбест, Ревда">Асбест, Ревда</option>
					<option value="Свердловская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Екатеринбург">Екатеринбург</option>
					<option value="Березовский, Верхняя Пышма, Новоуральск, Первоуральск">Березовский, Верхняя Пышма, Новоуральск, Первоуральск</option>
				</optgroup>
				<optgroup label="Смоленская область">
					<option value="Смоленск">Смоленск</option>
					<option value="Смоленская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Вязьма, Рославль, Сафоново, Ярцево">Вязьма, Рославль, Сафоново, Ярцево</option>
				</optgroup>
				<optgroup label="Ставропольский край">
					<option value="Кисловодск, Михайловск, Ставрополь">Кисловодск, Михайловск, Ставрополь</option>
					<option value="Ставропольский край - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Буденновск, Георгиевск, Ессентуки, Минеральные воды, Невинномысск, Пятигорск">Буденновск, Георгиевск, Ессентуки, Минеральные воды, Невинномысск, Пятигорск</option>
				</optgroup>
				<optgroup label="Тамбовская область">
					<option value="Тамбов">Тамбов</option>
					<option value="Мичуринск">Мичуринск</option>
					<option value="Тамбовская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Тверская область">
					<option value="Вышний Волочек, Кимры, Ржев">Вышний Волочек, Кимры, Ржев</option>
					<option value="Тверская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Тверь">Тверь</option>
				</optgroup>
				<optgroup label="Томская область">
					<option value="Северск">Северск</option>
					<option value="Томск">Томск</option>
					<option value="Томская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Тульская область">
					<option value="Узловая, Щекино">Узловая, Щекино</option>
					<option value="Алексин, Ефремов, Новомосковск">Алексин, Ефремов, Новомосковск</option>
					<option value="Тула">Тула</option>
					<option value="Тульская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Тюменская область">
					<option value="Тюменская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Тобольск">Тобольск</option>
					<option value="Тюмень">Тюмень</option>
				</optgroup>
				<optgroup label="Удмуртская Республика">
					<option value="Воткинск">Воткинск</option>
					<option value="Глазов, Сарапул">Глазов, Сарапул</option>
					<option value="Ижевск">Ижевск</option>
					<option value="Удмуртская Республика - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Ульяновская область">
					<option value="Димитровград">Димитровград</option>
					<option value="Ульяновск">Ульяновск</option>
					<option value="Ульяновская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Хабаровский край">
					<option value="Хабаровск">Хабаровск</option>
					<option value="Амурск">Амурск</option>
					<option value="Хабаровский край - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Комсомольск-на-Амуре">Комсомольск-на-Амуре</option>
				</optgroup>
				<optgroup label="Ханты-Мансийский автономный округ">
					<option value="Ханты-Мансийский автономный округ - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Когалым">Когалым</option>
					<option value="Нижневартовск">Нижневартовск</option>
					<option value="Нефтеюганск, Нягань">Нефтеюганск, Нягань</option>
					<option value="Ханты-Мансийск">Ханты-Мансийск</option>
					<option value="Сургут">Сургут</option>
				</optgroup>
				<optgroup label="Челябинская область">
					<option value="Магнитогорск">Магнитогорск</option>
					<option value="Сатка, Чебаркуль">Сатка, Чебаркуль</option>
					<option value="Челябинская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Златоуст, Миасс">Златоуст, Миасс</option>
					<option value="Копейск">Копейск</option>
					<option value="Челябинск">Челябинск</option>
				</optgroup>
				<optgroup label="Чеченская Республика">
					<option value="Чеченская Республика">Чеченская Республика</option>
				</optgroup>
				<optgroup label="Чувашская Республика">
					<option value="Новочебоксарск">Новочебоксарск</option>
					<option value="Канаш">Канаш</option>
					<option value="Чебоксары">Чебоксары</option>
					<option value="Чувашская Республика - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>
				<optgroup label="Чукотский автономный округ">
					<option value="Чукотский автономный округ">Чукотский автономный округ</option>
				</optgroup>
				<optgroup label="Ямало-Ненецкий автономный округ">
					<option value="Ноябрьск">Ноябрьск</option>
					<option value="Ямало-Ненецкий автономный округ - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
					<option value="Новый Уренгой">Новый Уренгой</option>
				</optgroup>
				<optgroup label="Ярославская область">
					<option value="Ярославль">Ярославль</option>
					<option value="Ярославская область - Прочие города и населенные пункты">Прочие города и населенные пункты</option>
				</optgroup>        
			</select>
			
			<select name="insurancePeriod" id="insurancePeriod" data-placeholder="Период использования">
				<option label="-"></option>				
				<option value="12 месяцев">12 месяцев</option>
				<option value="11 месяцев">11 месяцев</option>
				<option value="10 месяцев">10 месяцев</option>
				<option value="9 месяцев">9 месяцев</option>
				<option value="8 месяцев">8 месяцев</option>
				<option value="7 месяцев">7 месяцев</option>
				<option value="6 месяцев">6 месяцев</option>
				<option value="5 месяцев">5 месяцев</option>
				<option value="4 месяца">4 месяца</option>
				<option value="3 месяца">3 месяца</option>        
			</select>
			
			<select name="carType" id="carType" data-placeholder="Тип ТС">
				<option label="-"></option>				
				<option value="Мотоциклы и мотороллеры">Мотоциклы и мотороллеры</option>
				<option value="Легковые а/м">Легковые а/м</option>
				<option value="Легковые а/м, используемые в такси">Легковые а/м, используемые в такси</option>
                <option value="Грузовые а/м с разрешенной массой до 16 т вкл.">Грузовые а/м с разрешенной массой до 16 т вкл.</option>
				<option value="Грузовые а/м с разрешенной массой свыше 16 т">Грузовые а/м с разрешенной массой свыше 16 т</option>
                <option value="Автобусы, используемые в такси">Автобусы, используемые в такси</option>
			    <option value="Автобусы с числом мест сидения до 20 вкл.">Автобусы с числом мест сидения до 16 вкл.</option>
				<option value="Автобусы с числом мест сидения свыше 20">Автобусы с числом мест сидения свыше 16</option>
				<option value="Троллейбусы">Троллейбусы</option>
				<option value="Трамваи">Трамваи</option>
				<option value="Тракторы, дорожно-строительные и иные машины">Тракторы, дорожно-строительные и иные машины</option>                  
			</select>
			<select name="power" id="power" data-placeholder="Мощность">
				<option label="-"></option>	
				<option value="40">до 50 л.с.</option>
				<option value="60">51-70 л.с.</option>
				<option value="90">71-100 л.с.</option>
				<option value="110">101-120 л.с.</option>
				<option value="130">121-150 л.с.</option>
				<option value="160">свыше 150 л.с.</option>				
			</select>
			
			<div class="clear"></div>						
			<!--<p class="warranty">Владелец:</p>
			<label class="warranty-label"><input type="radio" name="owner" value="Юридическое лицо" />Юр.лицо</label>
			<label class="warranty-label"><input type="radio" name="owner" value="Физическое лицо" />Физ.лицо</label>	
			<div class="clear"></div>
			<br/>		-->	
			<p class="warranty">Ограничение числа лиц, допущенных к управлению:</p>
			<label class="credit-label"><input type="radio" name="multidrive" value="0" />Да</label>
			<label class="credit-label"><input type="radio" name="multidrive" value="1" />Нет</label>
		</div>
		<div class="clear"></div>
		<div class="kasko-driver">
			<p class="calc-title-driver">Водитель</p>
			<div class="driver">
				<input type="text" name="age[]" value="" placeholder="Возраст" />
				<input type="text" name="experience[]" value="" placeholder="Стаж" />
				<label class="sex-label"><input type="radio" name="driver_0_sex" value="m" />М</label>
				<label class="sex-label"><input type="radio" name="driver_0_sex" value="w" />Ж</label>
			</div>
			
			<span class="add-driver">+добавить водителя</span>
		</div>
		<div class="clear"></div>
		<div id="price_block">
			<input class="button" type="submit" name="send" value="Рассчитать" />
			<p class="price-label">Стоимость полиса от</p>
			<p class="price"><?echo isset($minKasko) ? $minKasko : '0';?></p>
			<input type="hidden" name="min_price_osago" id="min_price_osago" value="<?echo isset($minKasko) ? $minKasko : '0';?>" />
			<section class="currency"><img src="<?=SITE_TEMPLATE_PATH?>/images/2-inv_03.png" alt="" /><span class="show-note">*</span>
			<p class="note">
				Данный расчёт носит исключительно информативный характер. В расчёте не учтены такие параметры как: франшиза, порядок оплаты, переход из одной компании в другую. Данные параметры могут снизить стоимость Вашего полиса. Разобраться в тонкостях и деталях полиса поможет наш менеджер. Вам осталось заполнить контакты и нажать "Заказать полис" и мы свяжемся с Вами в течение 5 минут (в рабочее время).
				<span class="close-note"></span>
				<span class="triangle-note"></span>
			</p>
			</section>
		</div>						
	</form>	
	<div class="preload"></div>	
</div>		

<script>
$('#kasko_calc form').on('submit', function(){
	post = $(this).serializeArray();
	$(this).css('opacity', 0.5);
	$('.preload').fadeIn();	
	$.ajax({
		url: location.href,
		type: 'POST',
		data: post,
		success: function (data) {
			price = $(data).find('.price').html();
			min_price = $(data).find('#min_price_osago').val();
			err = $(data).find('#err_block').html();				
			if(err){
				alert(err);
			}else{
				$('.price').html(price);
				$('#min_price_osago').val(min_price);
				$('.note').fadeIn();
			}
			$('#kasko_calc form').css('opacity', 1);
			$('.preload').fadeOut();	
		},
		error: function (data) {
			console.log(data);
		}
	});
		
	return false;
})

$('#city').on('change', function(){
	var label=$('#city :selected').parent().attr('label'),
		city=$('#city :selected').val();
	if(label){
		$('#region').val(label);
	}else{
		$('#region').val(city);
	}
		
})	
</script>