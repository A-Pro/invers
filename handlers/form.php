<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

/*print_r(json_encode($_POST));

exit();*/

$message= "Имя: ".$_POST['name']."<br/>";
$message.= "Телефон: ".$_POST['phone']."<br/>";
$message.= "E-mail: ".$_POST['email']."<br/><br/>";

if(isset($_POST['min_price_kasko']) && $_POST['min_price_kasko'] > 0){	
	/*КАСКО*/
	$message.= "Данные расчёта по КАСКО<br/><br/>";
	$message.= "Марка: ".$_POST['make']."<br/>";
	$message.= "Модель: ".$_POST['model']."<br/>";
	$message.= "Год выпуска: ".$_POST['year']."<br/>";
	$message.= "Мощность: ".$_POST['power']."<br/>";
	$message.= "Цена: ".(int)$_POST['price']."<br/>";
	$message.= "В кредит: ".(boolean)$_POST['credit']."<br/>";
	$message.= "На гарантии: ".(boolean)$_POST['carGarantee']."<br/><br/>";	
	
	$message.= "Водители:<br/><br/>";
	
	foreach($_POST['age'] as $k=>$v){		
		$message.= "Возраст: " . $v . "<br/>";
		$message.= "Стаж: " . $_POST['experience'][$k] . "<br/>";
		$message.= "Пол: " . $_POST['driver_'.$k.'_sex'] . "<br/><br/>";				
	}
	
	$message.= "<br/>Расчётная стоимость: " . $_POST['min_price_kasko'];
}

if(isset($_POST['min_price_osago']) && $_POST['min_price_osago'] > 0){	
	/*ОСАГО*/
	$message.= "Данные расчёта по ОСАГО<br/><br/>";
	$message.= "Регион: ".$_POST['region']."<br/>";
	$message.= "Город: ".$_POST['city']."<br/>";
	$message.= "Период использования: ".$_POST['insurancePeriod']."<br/>";
	$message.= "Тип ТС: ".$_POST['carType']."<br/>";
	$message.= "Мощность: ".(int)$_POST['power']."<br/>";
	$message.= "Мультидрайв: ".(boolean)$_POST['multidrive']."<br/><br/>";
	
	$message.= "Водители:<br/><br/>";
	
	foreach($_POST['age'] as $k=>$v){		
		$message.= "Возраст: " . $v . "<br/>";
		$message.= "Стаж: " . $_POST['experience'][$k] . "<br/>";
		$message.= "Пол: " . $_POST['driver_'.$k.'_sex'] . "<br/><br/>";				
	}
	
	$message.= "<br/>Расчётная стоимость: " . $_POST['min_price_osago'];
}

$body = $message;

$arFields = Array(	
	"CONTENT"=>$body	
);

$eventName = "SEND_FORM";
							
$event = new CEvent;
			
if($sendit = $event->SendImmediate($eventName, SITE_ID, $arFields, "N", 25)){				
	$response = array(
		'error' => false
	);
	print_r(json_encode($response));
}else{
	$response = array(
		'error' => true,
		'err_message' => 'Ошибка отправки. Проверьте настройки сервера.'
	);
	print_r(json_encode($response));
}	