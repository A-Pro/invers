<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?php

//форма рекламации
if(isset($_POST['rekl_organization'])){
	?><pre><?//print_r($_POST)?></pre><?
	?><pre><?//print_r($_FILES)?></pre><?	
	/*класс для отправки файлов в init.php*/
	
	
	//основные данные
	$organization = $_POST['rekl_organization'];
	$city = $_POST['rekl_city'];
	$contact = $_POST['rekl_contact'];
	$mail = $_POST['rekl_mail'];
	$phone = $_POST['rekl_phone'];
	$date = $_POST['rekl_date'];
	
	//обработка добавляемых данных
	$arKeys = array();
	foreach($_POST as $k=>$v){
		$TmpK = explode('_', $k);
		if($TmpK[3] && !in_array($TmpK[3], $arKeys)){
			$arKeys[] = $TmpK[3];
		}		
	}
	sort($arKeys);
	
	//массив полей по каждому товару
	$arRes = array();
	$newFile = new VFile;
	
	for($i=1; $i<=count($arKeys); $i++){
		$arTmp = array(
			'item_name'=>$_POST['rekl_item_name_'.$i],
			'last_num'=>$_POST['rekl_last_num_'.$i],
			'issue_num'=>$_POST['rekl_issue_num_'.$i],
			'def_num'=>$_POST['rekl_def_num_'.$i],
			'all_num'=>$_POST['rekl_all_num_'.$i],
			'def_desc'=>$_POST['rekl_def_desc_'.$i],
			'def_cause'=>$_POST['rekl_def_cause_'.$i],
			'def_moment'=>$_POST['rekl_def_moment_'.$i],
			'first_on'=>$_POST['rekl_first_on_'.$i],
			'stop_after'=>$_POST['rekl_stop_after_'.$i],
			'stop_days'=>$_POST['rekl_stop_days_'.$i]
		);
		$arRes['data'][] = $arTmp; 
		
		//загрузка файлов
		$inputName = 'rekl_def_photo_'.$i;
		$Uid = rand(100, 999999);//можно использовать для уникализации папки
		$count = count($_FILES['rekl_def_photo_'.$i]['name']);	
		$subdir = '/upload/reclamation_files/';
		$filename = $newFile->FullUpload($inputName, $Uid, $count, $subdir);
		$arRes['files'][] = $filename;		
		
		if($filename == 'error'){
			$err = true;
		}
		
	}
	//конец добавляемых данных
	
	//Добавляем файлы из статичных полей
		$arInput = array(
		'rekl_def_brak', 'rekl_def_boy', 'rekl_def_else'
		);
		foreach($arInput as $input){
			$inputName = $input;
			$Uid = rand(100, 999999);//можно использовать для уникализации папки
			$count = count($_FILES['rekl_def_brak']['name']);
			$subdir = '/upload/reclamation_files/';			
			$filename = $newFile->FullUpload($inputName, $Uid, $count, $subdir);
			$arRes['files']['dop'][$input] = $filename;
			
			if($filename == 'error'){
				$err = true;
			}
		}
	
	//vars
	$content = '<!DOCTYPE html><html><body>';
	$content = $content.'<h1>Рекламация</h1><p>';
	if($organization!==''){
		$content = $content.'Организация: '.$organization.'<br/>';
	}
	if($city!==''){
		$content = $content.'Город : '.$city.'<br/>';
	}
	if($contact!==''){
		$content = $content.'Контактное лицо : '.$contact.'<br/>';
	}
	if($mail!==''){
		$content = $content.'Почта : '.$mail.'<br/>';
	}
	if($phone!==''){
		$content = $content.'Телефон : '.$phone.'<br/>';
	}
	if($date!==''){
		$content = $content.'Дата покупки : '.$date.'<br/>';
	}
	$content = $content.'</p>';
	$content = $content.'<h3>Товары:</h3><p>';
	foreach($arRes['data'] as $k=>$v){
		$content = $content.'Наименование: '.$v['item_name'].'<br/>';
		$content = $content.'Последние цифры спецификации: '.$v['last_num'].'<br/>';
		$content = $content.'Номер партии: '.$v['issue_num'].'<br/>';
		$content = $content.'Количество с дефектом: '.$v['def_num'].'<br/>';
		$content = $content.'Количество всего: '.$v['all_num'].'<br/>';
		if($v['first_on']=='on'){
			$content = $content.'Не работает при первом включении: Да<br/>';
		}
		if($v['stop_after']=='on'){
			$content = $content.'Перестал работать через: '.$v['stop_days'].' дней<br/>';
		}
		$content = $content.'Описание дефекта: '.$v['def_desc'].'<br/>';
		$content = $content.'Возможные причины: '.$v['def_cause'].'<br/>';
		$content = $content.'Момент обнаружения: '.$v['def_moment'].'<br/><br/>';
		$content = $content.'Файлы: ';
		foreach($arRes['files'][$k] as $file){
			$content = $content.'<br/>'.$file;
		}
		$content = $content.'</p>';
		$content = $content.'<p>';
	}
	$content = $content.'</p>';
	$content = $content.'<h4>Дополнительные файлы:</h4>';
	$content = $content.'<p>';
	foreach($arRes['files']['dop'] as $k=>$dopfiles){
		foreach($dopfiles as $dopfile){
			$content = $content.' '.$dopfile.'<br/>';
		}
	}
	$content = $content.'</p>';
	
	$content = $content.'</body></html>';
	
	//собираем файлы
	$path = '/upload/reclamation_files/';
	
	
	$allFiles = '';
	
	foreach($arRes['files']['dop'] as $k=>$dopfiles){
		foreach($dopfiles as $k=>$dopfile){
			$allFiles .= $path.$dopfile.';';
		}
	}
	foreach($arRes['data'] as $k=>$v){
		foreach($arRes['files'][$k] as $file){
			$allFiles .= $path.$file.';';
		}
	}
	$allFiles = substr($allFiles, 0, -1);
	
	
		
	//Отправка на почту
	
	$arFields = Array(	
		"CONTENT"=>$content,		
		"FILE_1_1"=>$allFiles,		
	);

	$eventName = "SEND_RECLAMATION";
					
	$event = new CEvent;
	
	if(!$err){
		if($event->Send($eventName, SITE_ID, $arFields, "N")){				
			$success = 'Y';
		}	
	}	

}

?>